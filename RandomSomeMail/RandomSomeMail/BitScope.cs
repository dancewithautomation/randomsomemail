﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dance.RandomSomeMail
{
    /// <summary>
    /// Indicate the value scope of a byte  
    /// </summary>
    public class BitScope
    {
        public int Min { get; set; }
        public int Max { get; set; }

        public BitScope()
        { }

        public BitScope(int min, int max)
        {
            Min = min;
            Max = max;
        }

        /// <summary>
        /// Produce a random value
        /// </summary>
        /// <returns>integer random value</returns>
        public int NextRandomValue()
        {
            if (Min == Max)
            {
                return Min;
            }

            Random rnd = new Random(ValidRandomSeed.GetInstance().NextSeed());
            return rnd.Next(Min, Max + 1);
        }
    }
}
