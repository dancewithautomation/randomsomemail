﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Dance.RandomSomeMail
{
    /// <summary>
    /// Use for randoming word based on the input of a dictionary
    /// </summary>
    public class WordRandomer:RandomerBase
    {
        private static string[] _lines;
        public WordRandomer()
        {
            string file = Environment.CurrentDirectory + @"\word.txt";
            _lines = File.ReadAllLines(file);

            for (int i = 0; i < _lines.Length; i++)
            {
                int index = _lines[i].IndexOf('[');
                if (index > 0)
                {
                    _lines[i] = _lines[i].Remove(index);
                }
            }
        }

        public override string CreateString(int length)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < length; i++)
            {
                Random rnd = new Random(ValidRandomSeed.GetInstance().NextSeed());
                int index = rnd.Next(0, _lines.Length - 1);
                sb.Append(_lines[index] + " ");
            }
            return sb.ToString();
        }
    }
}
