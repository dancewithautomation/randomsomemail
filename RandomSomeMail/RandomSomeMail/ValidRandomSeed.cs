﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dance.RandomSomeMail
{
    /// <summary>
    /// Use for seed randoming
    /// </summary>
    public class ValidRandomSeed
    {
        private static ValidRandomSeed _instance = null;
        private static int _additionalFactor = 0;
        private ValidRandomSeed()
        { }

        public static ValidRandomSeed GetInstance()
        {
            if (_instance == null)
            {
                _instance = new ValidRandomSeed();
            }
            return _instance;
        }

        public int NextSeed()
        {
            _additionalFactor++;
            int seed = System.Environment.TickCount+_additionalFactor;
            if (seed == int.MinValue)
            {
                seed = _additionalFactor;
            }
            return seed;
        }
    }
}
