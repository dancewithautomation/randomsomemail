﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace Dance.RandomSomeMail
{
    public class XmlSerializerHelper<T> where T : class
    {
        public static void ToFile(T value, string url)
        {
            using (System.IO.StreamWriter writer = new System.IO.StreamWriter(url))
            {
                XmlSerializer x = new XmlSerializer(typeof(T));
                x.Serialize(writer, value);
                writer.Close();
            }
        }

        public static T Load(string url)
        {
            using (System.IO.StreamReader reader = new System.IO.StreamReader(url))
            {
                XmlSerializer x = new XmlSerializer(typeof(T));
                T t = (T)x.Deserialize(reader);
                reader.Close();
                return t;
            }
        }

    }
}
