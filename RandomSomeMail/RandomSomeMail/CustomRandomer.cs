﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dance.RandomSomeMail
{
    /// <summary>
    /// Indicate a custom randomer
    /// </summary>
    public class CustomRandomer : RandomerBase
    {
        /// <summary>
        /// Randomer name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Byte scopes
        /// </summary>
        public List<BitScope> BitScopes { get; set; }

        public CustomRandomer()
        { }

        public CustomRandomer(string name,IList<BitScope> scopes)
        {
            if (scopes==null || scopes.Count != 4)
            {
                throw new ArgumentException("the scope list should contain 4 scope element");
            }
            Name = name;
            BitScopes = new List<BitScope>(scopes);
        }

        public CustomRandomer(string name, BitScope scope1,BitScope scope2,BitScope scope3,BitScope scope4)
        {
            Name = name;
            BitScopes = new List<BitScope>();
            BitScopes.Add(scope1);
            BitScopes.Add(scope2);
            BitScopes.Add(scope3);
            BitScopes.Add(scope4);
        }

        protected override List<byte[]> GeneralizeBytes(int length)
        {
            Random rnd = new Random();
            List<byte[]> bytesList = new List<byte[]>();
            int scope1, scope2, scope3, scope4;
            char char1, char2, char3, char4;
            for (int i = 0; i < length; i++)
            {
                scope1 = BitScopes[0].NextRandomValue();
                scope2 = BitScopes[1].NextRandomValue();
                scope3 = BitScopes[2].NextRandomValue();
                scope4 = BitScopes[3].NextRandomValue();

                char1 = BaseChar[scope1];
                char2 = BaseChar[scope2];
                char3 = BaseChar[scope3];
                char4 = BaseChar[scope4];

                byte byte1 = Convert.ToByte(char1.ToString() + char2.ToString(), 16);
                byte byte2 = Convert.ToByte(char3.ToString() + char4.ToString(), 16);
                byte[] byteArray = new byte[] { byte2, byte1 };
                bytesList.Add(byteArray);
            }

            return bytesList;
        }
    }
}
