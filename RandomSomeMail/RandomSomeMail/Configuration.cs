﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dance.RandomSomeMail
{
    /// <summary>
    /// Indicate Mail Random Option
    /// </summary>
    public class MailRandomOption
    {
        /// <summary>
        /// Set the folder to put random mails into
        /// </summary>
        public string GenerateFolder { get; set; }

        /// <summary>
        /// Set the subject length you want to random
        /// </summary>
        public int SubjectLength { get; set; }

        /// <summary>
        /// Set the content length you want to random
        /// </summary>
        public int ContentLength { get; set; }

        /// <summary>
        /// Set how many mails you want to random
        /// </summary>
        public int Count { get; set; }

        public MailRandomOption()
        { }

        public MailRandomOption(int count, int subjectLength, int contentLength, string folder)
        {
            Count = count;
            SubjectLength = subjectLength;
            ContentLength = contentLength;
            GenerateFolder = folder;
        }
    }

    /// <summary>
    /// Special Randomer put here
    /// </summary>
    public class SpecialRandomerList
    {
        public GB2312Randomer GB2312Randomer { get; set; }
        public WordRandomer WordRandomer { get; set; }
    }

    /// <summary>
    /// Indicate Configuration file structure for serializing
    /// </summary>
    public class Configuration
    {
        public MailRandomOption MailOption { get; set; }
        public List<CustomRandomer> CustomRandomers{ get; set; }
        public SpecialRandomerList SpecialRandomers { get; set; }

    }
}
