﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Mail;
using System.IO;

namespace Dance.RandomSomeMail
{
    public abstract class RandomerBase
    {
        protected static char[] BaseChar = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
        private static int _counter = 0;

        /// <summary>
        /// Random a string of characters(in form of byte array) by ginven length
        /// </summary>
        /// <param name="length">charater number to random</param>
        /// <returns>a list of byte array,each byte array indicate a character of some encoding</returns>
        protected virtual List<byte[]> GeneralizeBytes(int length)
        {
            return null;
        }

        /// <summary>
        /// Random a string of characters(in form of byte array) by ginven length and encoding
        /// </summary>
        /// <param name="length">charater number to random</param>
        /// <param name="en">encoding info</param>
        /// <returns>a list of byte array,each byte array indicate a character of some encoding</returns>
        protected string CreateString(int length, Encoding en)
        {
            List<byte[]> bytes = GeneralizeBytes(length);
            StringBuilder sb = new StringBuilder();
            foreach (byte[] ba in bytes)
            {
                sb.Append(en.GetString(ba));
            }
            return sb.ToString();
        }

        /// <summary>
        /// Random a string of characters(in form of byte array) by ginven length
        /// </summary>
        /// <param name="length">charater number to random</param>
        /// <returns>a list of byte array,each byte array indicate a character of some encoding</returns>
        public virtual string CreateString(int length)
        {
            Encoding en = Encoding.Unicode;
            return CreateString(length, en);
        }

        /// <summary>
        /// Create mail
        /// </summary>
        /// <param name="option">mail random option</param>
        public void CreateMail(MailRandomOption option)
        {
            try
            {
                int subjectLength = option.SubjectLength;
                int contentLength = option.ContentLength;

                for (int i = 0; i < option.Count; i++)
                {
                    Console.WriteLine("creating " + _counter++.ToString());
                    MailMessage mail = new MailMessage();
                    mail.Body = CreateString(contentLength);
                    mail.Subject = CreateString(subjectLength);
                    mail.From = new MailAddress("test@test.com");
                    mail.To.Add(new MailAddress("test@test.com"));
                    SmtpClient client = new SmtpClient("127.0.0.1");
                    client.DeliveryMethod = SmtpDeliveryMethod.SpecifiedPickupDirectory;
                    client.PickupDirectoryLocation =option.GenerateFolder;
                    if (!Directory.Exists(client.PickupDirectoryLocation))
                    {
                        Directory.CreateDirectory(client.PickupDirectoryLocation);
                    }
                    client.Send(mail);

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }


        }
    }
}
