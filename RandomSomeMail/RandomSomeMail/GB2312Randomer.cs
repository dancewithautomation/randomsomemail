﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dance.RandomSomeMail
{
    public class GB2312Randomer:RandomerBase
    {
        protected override List<byte[]> GeneralizeBytes(int length)
        {
            Random rnd = new Random(ValidRandomSeed.GetInstance().NextSeed());
            List<byte[]> byteList = new List<byte[]>();
            for (int i = 0; i < length; i++)
            {
                int scope1 = rnd.Next(11, 14);
                char char1 = BaseChar[scope1];

                
                int scope2;
                char char2;
                rnd = new Random(ValidRandomSeed.GetInstance().NextSeed());
                if (scope1 == 13)
                {
                    scope2 = rnd.Next(0, 7);
                }
                else
                {
                    scope2 = rnd.Next(0, 16);
                }
                char2 = BaseChar[scope2];

                int scope3 = rnd.Next(10, 16);
                char char3;
                rnd = new Random(ValidRandomSeed.GetInstance().NextSeed());
                char3 = BaseChar[scope3];
                
                int scope4;
                char char4;
                rnd = new Random(ValidRandomSeed.GetInstance().NextSeed());
                if (scope3 == 10)
                {
                    scope4 = rnd.Next(1, 16);
                }
                else if (scope3 == 15)
                {
                    scope4 = rnd.Next(0, 15);
                }
                else
                {
                    scope4 = rnd.Next(0, 16);
                }
                char4 = BaseChar[scope4];
     

                byte byte1 = Convert.ToByte(char1.ToString() + char2.ToString(), 16);
                byte byte2 = Convert.ToByte(char3.ToString() + char4.ToString(), 16);
                byte[] byteArray = new byte[] { byte1, byte2 };
                byteList.Add(byteArray);
            }

            return byteList;
        }

        public override string CreateString(int length)
        {
            Encoding en = Encoding.GetEncoding("gb2312");
            return CreateString(length, en);
        }
    }
}
