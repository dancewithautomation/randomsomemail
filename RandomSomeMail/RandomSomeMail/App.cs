﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Dance.RandomSomeMail
{
    internal class App
    {
        public static Configuration Conf;

        static void Main(string[] args)
        {
            try
            {
                //dis-serialize the configuration file
                string confFile = Environment.CurrentDirectory + @"\configuration.xml";
                if (!File.Exists(confFile))
                {
                    throw new FileNotFoundException("configuration file not found");
                }
                Conf = XmlSerializerHelper<Configuration>.Load(confFile);

                List<RandomerBase> all = new List<RandomerBase>();
                if (Conf.CustomRandomers != null)
                {
                    foreach (CustomRandomer custom in Conf.CustomRandomers)
                    {
                        all.Add(custom);
                    }
                }

                if (Conf.SpecialRandomers != null)
                {
                    if (Conf.SpecialRandomers.GB2312Randomer != null)
                    {
                        all.Add(Conf.SpecialRandomers.GB2312Randomer);
                    }

                    if (Conf.SpecialRandomers.WordRandomer != null)
                    {
                        all.Add(Conf.SpecialRandomers.WordRandomer);
                    }
                }

                //enumerate each randomer and create mail
                foreach (RandomerBase random in all)
                {
                    random.CreateMail(Conf.MailOption);
                }

                Console.WriteLine("press <ENTER> to exit...");
                Console.ReadLine();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            

        }
    }
}
